# pins #

This is a simple program that allows you to create interesting connection graphics from an input image. It was designed to help organize making a physical version.

It is inspired by work such as [this](http://senseslost.com/2012/04/19/negative-space-typography-using-pins-string/).