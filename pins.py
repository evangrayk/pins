#!/usr/bin/env python
# -*-encoding:UTF-8-*-

from PIL import Image, ImageDraw
import math
import sys

def progress(count, l, length=40, COL='\033[32m'):
    '''
    print progress bar, length characters long,
    in COL (ansi escape color sequence)
    with position count/l
    '''
    l = l-1
    p = int((float(count) / l) * length)
    done = '=' * p
    notdone = ' ' * (length - p)
    CLR = '\033[0m'
    s = "[%s%s%s%s] %04d/%04d" % (COL, done, CLR, notdone, count, l)
    sys.stdout.write('\033[0G')
    sys.stdout.flush()
    sys.stdout.write(s)


def distance(a, b):
    dx = a.x - b.x
    dy = a.y - b.y
    return math.sqrt(dx*dx + dy*dy)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self, drawer, upscale=1):
        drawer.point([upscale*x, upscale*y])

class Line:
    def __init__(self, P1, P2, dist=-1):
        self.p1 = P1
        self.p2 = P2
        if dist == -1:
            dist = distance(P1, P2)
        self.d = dist


    def draw(self, drawer, upscale=1, fill=None):
        drawer.line([upscale*self.p1.x, upscale*self.p1.y, upscale*self.p2.x, upscale*self.p2.y], fill=fill)

class Pin:
    def __init__(self, x, y):
        self.pt = Point(x,y)
        self.lines = []

    def add_line(self, l):
        self.lines.append(l)

    def draw(self, drawer, upscale=1):
        self.drawlines(drawer, upscale)
        self.drawpt(drawer, upscale)

    def drawlines(self, drawer, upscale=1, colors=[(255, 255, 255)]):
        import random
        for line in self.lines:
            line.draw(drawer, upscale, random.choice(colors))

    def drawpt(self, drawer, upscale=1):
        drawer.point([upscale*self.pt.x, upscale*self.pt.y], fill=(255, 0, 0))

    def __repr__(self):
        return "<Pin: (%f, %f)>" % (self.pt.x, self.pt.y)

    def remove_line(self, l):
        self.lines.remove(l)


    def can_add(self, d, max_connections):
        """
        this distance is either not the largest,
        or there is room for it anyway
        """
        if len(self.lines) >= max_connections:
            for l in self.lines:
                if l.d > d:
                    self.remove_line(l)
                    return True
        else:
            return True
        return False


RED=0
GREEN=1
BLUE=2

INSIDE = BLUE
OUTSIDE = GREEN
EDGE = RED
ON = 255

def ERROR(msg):
    print "[Error]", msg

class Mesh:
    '''
    hold data for the entire mesh created, including all the pins and options
    '''
    def __init__(self, map_file, block=OUTSIDE, max_dist=30, upscale=2, max_connections=-1, color=None):
        print "[Initializing]"
        print "    max dist: %d  upscale: %d  max connections: %d" % (max_dist, upscale, max_connections)
        if map_file == "":
            ERROR("Empty path entered")
            return
        img = Image.open(map_file)
        if not img:
            ERROR("Invalid file")
        self.map_file = img
        self.size = self.map_file.size
        self.check = block
        self.pins = []
        self.max_dist=max_dist
        self.max_connections=max_connections
        self.upscale=upscale
        if color == 'red':
            self.colors = [(244, 85, 73), (115, 8, 0), (242, 17, 0), (115, 40, 34), (119, 14, 0)]
        elif color == 'assorted':
            self.colors = [(159, 173, 191), (242, 233, 99), (242, 190, 92), (242, 139, 80), (242, 105, 92)]
        elif color == 'browns':
            self.colors = [(242, 233, 99), (242, 190, 92), (242, 139, 80), (242, 105, 92)]
        else:
            self.colors = [(255, 255, 255)]
        print "[Adding Points]"
        for x in range(self.size[0]):
            for y in range(self.size[1]):
                p = self.map_file.getpixel((x,y))
                if iscolor(p, EDGE):
                    self.pins.append(Pin(x,y))
        print "    added %d points" % (len(self.pins))
        print "[Making connections]"
        self._make_connections()
        self.canvas = Image.new('RGB', (self.size[0]*self.upscale, self.size[1]*self.upscale), (0, 0, 0))
        self.drawer = ImageDraw.Draw(self.canvas)

    def _make_connections(self):
        '''
        create Line objects between every pair of points
        that are close and aren't interrupted by barrier
        '''
        le = len(self.pins)
        for i, pin in enumerate(self.pins):
            progress(i, le)
            for pin2 in self.pins[i:]:
                d = distance(pin.pt, pin2.pt)
                if d == 0:
                    continue
                free = self.islinefree(pin.pt, pin2.pt, self.check)
                if d < self.max_dist and free:
                    l = Line(pin.pt, pin2.pt, dist=d)
                    if (self.max_connections < 0 or (pin.can_add(d, self.max_connections) and pin2.can_add(d, self.max_connections))):
                        pin.add_line(l)
                        #pin2.add_line(l) # don't need both directions
        print

    def draw(self):
        """
        draw the lines
        """
        print "[Drawing]"
        for pin in self.pins:
            pin.drawlines(self.drawer, self.upscale, self.colors)
            #pin.drawlines(self.drawer, self.upscale)
        #for pin in self.pins:
            #pin.drawpt(self.drawer, self.upscale)

    def show(self):
        self.draw()
        self.canvas.show()

    def save(self, path):
        self.draw()
        self.canvas.save(path, 'bmp')


    def islinefree(self, p1, p2, check=OUTSIDE):
        """
        go through every point between ends of a line
        and check if it is in the restricted zone
        Uses bresenham's line drawing algo
        return False if the path is blocked
        """
        x0 = p1.x
        x1 = p2.x
        y0 = p1.y
        y1 = p2.y

        dx = abs(x1 - x0)
        dy = abs(y1 - y0)
        x, y = x0, y0
        sx = -1 if x0 > x1 else 1
        sy = -1 if y0 > y1 else 1
        if dx > dy:
            err = dx / 2.0
            while x != x1:
                v = self.map_file.getpixel((x,y))
                if iscolor(v, check):
                    return False
                err -= dy
                if err < 0:
                    y += sy
                    err += dx
                x += sx
        else:
            err = dy / 2.0
            while y != y1:
                v = self.map_file.getpixel((x,y))
                if iscolor(v, check):
                    return False
                err -= dx
                if err < 0:
                    x += sx
                    err += dy
                y += sy
        return True;


def iscolor(v, check):
    """
    return true if v is the color specified by check
    0 is red, 1 is green, 2 is blue
    """
    l = 3
    a = (check + 0) % l
    b = (check + 1) % l
    c = (check + 2) % l
    if v[a] == ON and v[b] != ON and v[c] != ON:
        return True
    return False

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Generate line connections from a map.')
    parser.add_argument('file', type=str, help='Input map file. Red is a point, Green is outside, Blue is inside. White is ignored.')
    parser.add_argument('-o', type=str, metavar='output file', help='Set output file.')
    parser.add_argument('-d', type=int, metavar='max distance', help='Set maximum distance in pixels for a line.')
    parser.add_argument('-n', type=int, metavar='max connections', help='Set maximum number of lines going into same point.')
    parser.add_argument('-u', type=int, metavar='upscale', help='Set how much the resultant image should upscale.')
    parser.add_argument('-c', type=str, help='Set which color theme to use.', choices=['white', 'red', 'assorted', 'browns'])
    parser.add_argument('-i', action='store_true', help='Switch to allowing lines through green.')
    args = parser.parse_args()

    d = args.d
    u = args.u
    n = args.n
    i = args.i
    f = args.file
    o = args.o
    c = args.c
    theargs = {}
    if d:
        theargs['max_dist']=d
    if u:
        theargs['upscale']=u
    if n:
        theargs['max_connections']=n
    if c:
        theargs['color']=c
    if i:
        theargs['block']=INSIDE

    #m.show()
    m = Mesh(f, **theargs)
    m.save(o)
